#include "util/timers.h"

uint timers[2];
uint get_clock()
{
    return E_CTIMER_MAX - timers[0];
}

// #ifdef TIMER_ON_POLLING
// void timer_polling_resume()
// {
//     e_ctimer_set(E_CTIMER_1, E_CTIMER_CLK, timers[1]);
//     e_ctimer_start(E_CTIMER_1, E_CTIMER_CLK);
// }

// void timer_polling_pause()
// {
//     timers[1] = e_ctimer_get(E_CTIMER_1);
// }
// #endif

// #ifdef TIMER_ON_API
// void timer_api_resume()
// {
//     e_ctimer_set(E_CTIMER_1, E_CTIMER_CLK, timers[1]);
//     e_ctimer_start(E_CTIMER_1, E_CTIMER_CLK);
// }

// void timer_api_pause()
// {
//     timers[1] = e_ctimer_get(E_CTIMER_1);
// }
// #endif

// #ifdef TIMER_ON_TRANSACTION
// void timer_transaction_resume()
// {
//     e_ctimer_set(E_CTIMER_0, E_CTIMER_CLK, timers[0]);
//     e_ctimer_start(E_CTIMER_0, E_CTIMER_CLK);
// }

// void timer_transaction_pause()
// {
//     timers[0] = e_ctimer_get(E_CTIMER_0);
// }
// #endif


void init_timer(uchar id)
{
    timers[id] = E_CTIMER_MAX;
    switch(id) {
        case 0:
            e_ctimer_set(E_CTIMER_0, E_CTIMER_CLK, E_CTIMER_MAX);
            break;
        case 1:
            e_ctimer_set(E_CTIMER_1, E_CTIMER_CLK, E_CTIMER_MAX);
            break;
    }
}

uint get_time()
{
    return E_CTIMER_MAX - timers[1];
}
