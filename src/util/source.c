#include <stdbool.h>
#include "util/source.h"
#include "util/config.h"

bool network_not_finished(volatile network_source *p)
{
    return p->index < p->size;
}

V_TYPE network_read(volatile network_source *p)
{
    return p->array[p->index];
}

V_TYPE network_consume(volatile network_source *p)
{
    return p->array[p->index++];
}

#ifdef SKIP_SHARED_MEMORY
static int index[2] = {0, 0};
static int source[64] = {1458, -289, -249, -149, -169, -89, -49, 0, 49, 69, 0, 0, -29, -29, -49, 0, 89, -69, -29, -29, 49, 0, 0, 0, 69, -69, 0, 0, 0, 0, 0, 0, 69, 0, 0, -29, -29, 0, 0, 0, 0, 0, 0, -29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void network_init()
{
    index[0] = index[1] = 0;
}
bool network_not_finished_in()
{
    return index[0] < 64 * INPUT_REPEAT;
}
int network_read_in()
{
    return source[index[0] % 64];
}
int network_consume_in()
{
    return source[index[0]++ % 64];
}

bool network_not_finished_signed()
{
    return index[1] < 1 * INPUT_REPEAT;
}
int network_read_signed()
{
    return 0;
}
int network_consume_signed()
{
    index[1]++;
    return 0;
}

#endif
