#include <e_coreid.h>
#include <e_common.h>
#include "util/config.h"
#include "util/types.h"
#include "util/common.h"
#include "util/timers.h"

void connect_network();
volatile shared_dram dram SECTION(".shared_dram");
volatile actors *all;
volatile shared_buf_t *Mailbox;

inline unsigned core_num()
{
    e_coreid_t coreid = e_get_coreid();
    unsigned row, col;

    row = (coreid >> 6) & (E_ROWS_IN_CHIP - 1);
    col =  coreid       & (E_COLS_IN_CHIP - 1);
    return row * E_COLS_IN_CHIP + col;
}

void print_core_index(int i) {
    Mailbox->debug_index[core_num()] = i;
}

void inc_core_at_index(int i)
{
    Mailbox->debug[core_num()][i]++;
}

void print_core_at_index(int v, int i) {
    Mailbox->debug[core_num()][i] = v;
}

void print_core(int v) {
    unsigned char num = core_num();
    uint index = Mailbox->debug_index[num] + 4;
    if (index < 100) {
        print_core_at_index(v, index);
        Mailbox->debug_index[num]++;
    }
}


inline void *address_from_coreid(e_coreid_t coreid, volatile void *ptr)
{
    return e_address_from_coreid(coreid, (void *)ptr);
}

void stage(uint s)
{
    while(Mailbox->core.go[core_num()] < s) ;
}

void stage_all(uint s)
{
    int i;
    for (i=0; i<sizeof(Mailbox->is_active)/sizeof(Mailbox->is_active[0]); ++i) {
        if (Mailbox->is_active[i]) {
            while(Mailbox->core.go[i] < s) ;
        }
    }
}

void broadcast_all(uint s)
{
    int i;
    for (i=0; i<sizeof(Mailbox->is_active)/sizeof(Mailbox->is_active[0]); ++i) {
        if (Mailbox->is_active[i]) {
            Mailbox->core.go[i] = s;
        }
    }
}

// set by core0
volatile bool ready_to_start_timer = false;
bool* ready_to_start_timer_local[16];

void broadcast_timer()
{
    int i;
    for (i=sizeof(Mailbox->is_active)/sizeof(Mailbox->is_active[0]) - 1; i>=0; --i) {
        if (Mailbox->is_active[i]) {
            *ready_to_start_timer_local[i] = true;
        }
    }
}

static void init_ready_to_start_timer()
{
    Mailbox->core.ready_to_start_timer[core_num()] = address_from_coreid(e_get_coreid(), &ready_to_start_timer);
}
static void copy_ready_to_start_timer_local()
{
    int i;
    for (i = 0; i < 16; ++i) {
        if (Mailbox->is_active[i]) {
            while (!Mailbox->core.ready_to_start_timer[i]) ;
            ready_to_start_timer_local[i] = Mailbox->core.ready_to_start_timer[i];
        }
    }
}

volatile uint api_counter;
bool after_first_token;
int internal_helper[4] = { 0, 0, 0, 0 };

void restart_iteration()
{
    // API_COUNT
    int i;
    for (i = 0; i < 4; ++i)
    {
        Mailbox->debug[core_num()][i] = 0;
    }
    api_counter = 0;
    after_first_token = false;
    ready_to_start_timer = false;
    if (core_num() == 0) {
        Mailbox->n_source[0].index = Mailbox->n_source[1].index = 0;
        {
            int j;
            for ( j=0; j<sizeof(Mailbox->n_sink)/sizeof(Mailbox->n_sink[0]); ++j) {
                Mailbox->n_sink[j].index = 0;
            }
        }
#ifdef SKIP_SHARED_MEMORY
        network_init();
#endif
    }
}

void core_main(void *a, init_t *init)
{
    Mailbox = &dram.Mailbox;
    all = &dram.all;
    Mailbox->core.go[core_num()] = 0;
    print_core_index(0);
    if (core_num() == 0) {
        Mailbox->sink[4] = -1;
    }

    stage(1);
    init_ready_to_start_timer();

    int i;
    for ( i = 0; i < Mailbox->iteration; ++i ) {
        restart_iteration();
        api_t *api = init(a);

        Mailbox->core.go[core_num()] = 2;
        stage_all(2);

        if (core_num() == 0) {
            if ( i == 0 ) {
                copy_ready_to_start_timer_local();
            }
            connect_network();
            broadcast_all(3);
        }

        internal_helper[0] = core_num();
        internal_helper[1] = Mailbox->n_source[0].size;
        internal_helper[2] = 0;
        internal_helper[3] = Mailbox->last_core;
        init_timer(0);
        init_timer(1);
        stage(3);

        if (core_num() == 0) {
            broadcast_timer();
        }
        while (!ready_to_start_timer) ;
        timer_transaction_resume();
        timer_latency_resume();
        while(1) {
            api->run(a);
            api->run(a);
            if (!api->not_finished(a)) {
#ifdef LAYOUT_SNAKE
                if (internal_helper[0] == 1 || internal_helper[0] == 4) {
#endif
#ifdef LAYOUT_SEQUENCE
                if (internal_helper[0] == 1 || internal_helper[0] == 7) {
#endif
                    while(internal_helper[2] < internal_helper[1]*2) {
                        api->run(a);
                    }
                } else {
                    while(internal_helper[2] < internal_helper[1]) {
                        api->run(a);
                    }
                }
                break;
            }
        }
        api->end(a);
        timer_activation_pause();
        timer_transaction_pause();
        Mailbox->core.go[core_num()] = 4;
        Mailbox->core.cycles[core_num()] = get_time();
        Mailbox->core.clocks[core_num()] = get_clock();
        Mailbox->core.go[core_num()] = 5;
        Mailbox->core.ready[core_num()] = api_counter;
        while(Mailbox->core.go[core_num()] != 1) ;
    }
    while(1) ;
}
