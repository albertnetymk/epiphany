#include "util/sink.h"
#ifdef TIMER_ON_LATENCY
#include "util/timers.h"
#endif

extern int internal_helper[4];
void network_write(volatile network_sink *p, V_TYPE v)
{
#ifdef TIMER_ON_LATENCY
    static bool flag = true;
    if (flag && internal_helper[0] == internal_helper[3]) {
        flag = false;
        timer_latency_pause();
    }
#endif
    // inc_core_at_index(2);
    internal_helper[2]++;
#ifndef SKIP_SHARED_MEMORY
    p->array[p->index++] = v;
#endif
    // inc_core_at_index(3);
}
