#ifndef SOURCE_H_G9FRYOZW
#define SOURCE_H_G9FRYOZW

#include <stdbool.h>
#include "util/mailbox.h"
extern volatile shared_buf_t *Mailbox;
#include "util/config.h"
bool network_not_finished(volatile network_source *p);
V_TYPE network_read(volatile network_source *p);
V_TYPE network_consume(volatile network_source *p);

#ifdef SKIP_SHARED_MEMORY
void network_init();
bool network_not_finished_in();
int network_read_in();
int network_consume_in();
bool network_not_finished_signed();
int network_read_signed();
int network_consume_signed();
#endif

#endif /* end of include guard: SOURCE_H_G9FRYOZW */

