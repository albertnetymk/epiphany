#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <stdbool.h>
#include "util/types.h"
#include "util/flags.h"
#include "util/config.h"
#include <e_dma.h>

#define MIN(x, y) (x>y? y:x)

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 10
#endif

#ifndef V_TYPE
#define V_TYPE int
#endif

#ifdef USE_DESTINATION_BUFFER
struct port_out_struct;
typedef volatile struct port_in_struct {
    uint read_index;
    uint write_index;
    V_TYPE array[BUFFER_SIZE];
    bool carrier;
    bool end;
    volatile struct port_out_struct *origin;
} port_in;

typedef volatile struct port_out_struct {
    port_in *(*dests)[];
    uchar dest_index;
    bool carrier_mirror;
    uint read_index_mirror;
    uint write_index_mirror;
} port_out;

#else // USE_DESTINATION_BUFFER

enum dma_status {
    DMA_PENDING,
    DMA_ING,
    DMA_FINISHED,
    DMA_IDLE
};

typedef volatile struct dma_cfg_struct {
    enum dma_status status;
    e_dma_id_t id;
} dma_cfg;

typedef volatile struct fifo_struct {
    uint total;
    uint total_mirror;
    V_TYPE array[BUFFER_SIZE];
    uint size;
    volatile struct fifo_struct *twin;
    dma_cfg *dma;
} fifo;

#ifdef USE_BOTH_BUFFER
typedef struct port_in_struct {
    uint index;
    fifo *buffer;
    bool end;
} port_in;

typedef struct port_out_struct {
    uint index;
    fifo *buffer;
    port_in *(*dests)[];
    uchar dest_index;
} port_out;
#endif // USE_BOTH_BUFFER

#ifdef USE_DOUBLE_BUFFER
typedef struct port_in_struct {
    uint index;
    uchar ping_pang;
    fifo *buffers[2];
    bool end;
} port_in;

typedef struct port_out_struct {
    uint index;
    uchar ping_pang;
    fifo *buffers[2];
    port_in *(*dests)[];
    uchar dest_index;
    uchar current_dest_index[2];
} port_out;
#endif // USE_DOUBLE_BUFFER

#ifdef USE_MULTIPLE_BUFFER

typedef struct port_in_struct {
    uint index;
    uchar buffer_index;
    fifo *buffers[BUFFER_NUMBER];
    bool end;
} port_in;

typedef struct port_out_struct {
    uint index;
    uchar buffer_index;
    fifo *buffers[BUFFER_NUMBER];
    port_in *(*dests)[];
    uchar dest_index;
    uchar current_dest_index[BUFFER_NUMBER];
} port_out;
#endif // USE_MULTIPLE_BUFFER

#endif // USE_DESTINATION_BUFFER

void port_out_init(port_out *p);
void port_in_init(port_in *p);
void epiphany_write(port_out *p, V_TYPE v);
V_TYPE epiphany_read(port_in *p);
V_TYPE epiphany_peek(port_in *p);
void flush(port_out *p);
void end_port(port_out *p);
void connect(port_out *out, port_in *in);
bool has_input(port_in *p, uint n);
bool might_has_input(port_in *p);
V_TYPE ReadToken(port_in *p, uint n);
V_TYPE ConsumeToken(port_in *p, uint n);
void SendToken(port_out *p, V_TYPE v, uint n);
bool TestInputPort(port_in *p, uint n);
void dead();

#endif /* end of include guard: _COMMUNICATION_H */
