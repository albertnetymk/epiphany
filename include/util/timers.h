#ifndef TIMERS_H_9QZH7D5O
#define TIMERS_H_9QZH7D5O

#include <e_ctimers.h>
#include "util/types.h"

extern uint timers[2];
#ifdef TIMER_ON_POLLING
static inline void timer_polling_resume()
{
    e_ctimer_set(E_CTIMER_1, E_CTIMER_CLK, timers[1]);
}

static inline void timer_polling_pause()
{
    timers[1] = e_ctimer_get(E_CTIMER_1);
}
#else
static inline void timer_polling_resume() { }
static inline void timer_polling_pause() { }
#endif

#ifdef TIMER_ON_API
static inline void timer_api_resume()
{
    e_ctimer_set(E_CTIMER_1, E_CTIMER_CLK, timers[1]);
}

extern volatile uint api_counter;
static inline void timer_api_pause()
{
    timers[1] = e_ctimer_get(E_CTIMER_1);
    api_counter++;
}
#else
static inline void timer_api_resume() { }
static inline void timer_api_pause() { }
#endif

#ifdef TIMER_ON_LATENCY
static inline void timer_latency_resume()
{
    e_ctimer_set(E_CTIMER_0, E_CTIMER_CLK, timers[0]);
}

static inline void timer_latency_pause()
{
    timers[0] = e_ctimer_get(E_CTIMER_0);
}
#else
static inline void timer_latency_resume() { }
static inline void timer_latency_pause() { }
#endif

#ifdef TIMER_ON_TRANSACTION
static inline void timer_transaction_resume()
{
    e_ctimer_set(E_CTIMER_0, E_CTIMER_CLK, timers[0]);
}

static inline void timer_transaction_pause()
{
    timers[0] = e_ctimer_get(E_CTIMER_0);
}
#else
static inline void timer_transaction_resume() { }
static inline void timer_transaction_pause() { }
#endif

#ifdef TIMER_ON_ACTIVATION
static inline void timer_activation_resume()
{
    e_ctimer_set(E_CTIMER_0, E_CTIMER_CLK, timers[0]);
}

static inline void timer_activation_pause()
{
    timers[0] = e_ctimer_get(E_CTIMER_0);
}
#else
static inline void timer_activation_resume() { }
static inline void timer_activation_pause() { }
#endif

void init_timer(uchar id);
uint get_clock();
uint get_time();

#endif /* end of include guard: TIMERS_H_9QZH7D5O */

