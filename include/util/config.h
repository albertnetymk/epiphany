#ifndef CONFIG_H_YP4ZIQR0
#define CONFIG_H_YP4ZIQR0

// skip external memory access by using dummy input
#define SKIP_SHARED_MEMORY
#define INPUT_REPEAT 1000

// #define API_COUNT

// layout mapping
#define LAYOUT_SNAKE
// #define LAYOUT_SEQUENCE

// remote reading
// #define POLLING_ON_REMOTE
#define V_TYPE int

#define RUN_ITERATION 1

// There are only two timers on Epiphany, so only two defines could be used once.
// #define TIMER_ON_ACTIVATION
// #define TIMER_ON_POLLING
// #define TIMER_ON_API
#define TIMER_ON_LATENCY
// #define TIMER_ON_TRANSACTION


#if defined(TIMER_ON_API) || defined(TIMER_ON_POLLING)
#define PRINT_TIMER_PER_CORE
#endif
// #define INCLUDE_HAS_INPUT_IN_API_TIMER

#endif /* end of include guard: CONFIG_H_YP4ZIQR0 */
