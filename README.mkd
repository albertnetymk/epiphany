Simple FIFO communication between cores of [Epiphany](http://www.parallella.org/) architecture.

## Communication library
If you are interested in using this communication library in your hand crafted or generated code, you only need to look at `master` branch, which
(should) holds the latest skeleton of the communication library, with some synchronization boilerplate.

## Epiphany SDK version
This repo (building process) assumes you are using the SDK which contains `e-loader` command (older approach). There's no support for the new SDK yet.
No idea how the com library is affected by this.

## How to use this library on Epiphany
According to the name of this repo, one should not be surprised that this library is developed using Epiphany as the medium. (So far, it's the only
architecture this library runs on.) I will provide some info how it's used on Epiphany, which, hopefully, could shed some light on how to port it to
some other architecture.

### File organization

* `board`: This folder contains the code specific to Epiphany. Inside it, each folder is name as `core[coreid]`, for Epiphany requires one dedicated `ELF` for each core. `coordinates.c` is responsible for providing identity for each core (kind of reflection). `main.c` provides the entry for the code running on the core. Since dynamic memory management is too expensive to have on Epiphany, it's expected that in `main.c`, all the input/output ports are declared statically so that com library would only do some pointer manipulation.

* `src/util`: This folder contains some utilities that you might find it usable. Some of them is tied to Epiphany, for example `timers.c`. `common.c` is shared by all the cores on Epiphany to reduce the duplication in each core by using `Single Program Multiple Data(SPMD)`.

* `src/actors`: This com library is developed for running CAL (one dataflow language) on Epiphany, so this folder is here for holding the code for actors, which provides blueprint for creating instances (just class in some OOP languages). This organization could get rid of a lot of code duplication; how much this matters is only personal opinion.

* `host`: The host is preparing input data and fetching output data, with optional comparison with expect output. What host does is quite subjective, and it's one open question (maybe depending on the application) what logic shoud be placed here.

## How to run it on Epiphany
In the ideal case, `make` itself is enough, which performs the following steps in sequence:

1. Build code for the chip.
2. Loading the program to the chip, and run it.
3. Build code for host.
4. Running the program for host

## Applications (Branches)
In order to achieve isolated environment for every application, I create one branch for each app. In addition, there are some auxiliary branches as
well, which is why there are quite a few branches in this repo. Don't worry, for most of them are there merely for debugging purposes. The branches
that you might be interested in are `idct2d`, `matrix`, `fft` and `fir`. The following is one very brief introduction to each of them.

* `idct2d`: 2D-IDCT with 64000 input data samples
* `matrix`, `fft`, `fir`: The three apps all have the same connection pattern; the only difference lies on the operation each actor performs, namely,
dot product, FFT, FIR (special dot product), respectively.
